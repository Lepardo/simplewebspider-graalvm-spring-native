# simplewebspider-graalvm-spring-native

It's a playground to get familiar with graalvm and spring native.

## Setup developer environment

1. Install vscode
2. Install maven
3. Install [GraalVM](https://www.graalvm.org/downloads/) for Java 11
   1. Download [Microsoft Windows SDK for Windows 7 and .NET Framework 4 (ISO)](https://www.microsoft.com/en-us/download/details.aspx?id=8442)
   2. Execute `\Setup\SDKSetup.exe
4. clone repository

## Known issues

### Native build package is not working

```txt
$ ./mvnw -Pnative-image package
...
Error: Default native-compiler executable 'cl.exe' not found via environment variable PATH
Error: To prevent native-toolchain checking provide command-line option -H:-CheckToolchain
Error: Use -H:+ReportExceptionStackTraces to print stacktrace of underlying exception
Error: Image build request failed with exit status 1
```

## Further documents

* <https://github.com/spring-projects-experimental/spring-native>
  * <https://docs.spring.io/spring-native/docs/current/reference/htmlsingle/#getting-started-native-image>